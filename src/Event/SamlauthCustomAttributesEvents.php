<?php
/**
 * Created by IntelliJ IDEA.
 * User: jbaker
 * Date: 7/14/17
 * Time: 2:11 PM
 */

namespace Drupal\samlauth_custom_attributes\Event;


/**
 * Class SamlauthCustomAttributesEvents
 *
 * Defines the event name.
 *
 * @package Drupal\samlauth_custom_attributes\Event
 */
final class SamlauthCustomAttributesEvents {
  const CUSTOM_FIELD = 'samlauth_custom_attributes.custom_field';
}
